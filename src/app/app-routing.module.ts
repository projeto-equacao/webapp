import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InfoEquacaoComponent } from './page/info/info-equacao/info-equacao.component';
import { LancarEquacaoComponent } from './page/lancar/lancar-equacao/lancar-equacao.component';
import { ResultEquacaoComponent } from './page/result/result-equacao/result-equacao.component';

const routes: Routes = [
  { path: '', component: InfoEquacaoComponent},
  { path: 'lancar-equacao', component: LancarEquacaoComponent },
  { path: 'result-equacao', component: ResultEquacaoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
