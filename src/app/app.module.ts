import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { InfoEquacaoComponent } from './page/info/info-equacao/info-equacao.component';
import { LancarEquacaoComponent } from './page/lancar/lancar-equacao/lancar-equacao.component';
import { ResultEquacaoComponent } from './page/result/result-equacao/result-equacao.component';

@NgModule({
  declarations: [
    AppComponent,
    InfoEquacaoComponent,
    LancarEquacaoComponent,
    ResultEquacaoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
