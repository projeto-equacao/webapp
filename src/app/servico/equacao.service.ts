import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Inject, Injectable } from "@angular/core";

import { EquacaoRequest } from '../dto/request/equacao-request.dto';
import { EquacaoResponse } from '../dto/response/equacao-response.dto';


@Injectable({ 
    providedIn:"root"
})

export class EquacaoService {
    ResultEquacao() {
      throw new Error('Method not implemented.');
    }

    constructor(private http : HttpClient) {}

    calcular(request: EquacaoRequest): Observable<EquacaoResponse> {
        return this.http.post<EquacaoResponse>('/server/projetoEquacao/api/equacao', request)
    }
}