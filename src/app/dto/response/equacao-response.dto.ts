export interface EquacaoResponse
 {
    delta?: number,
    x1?: number,
    x2?: number,
    message?: string
}