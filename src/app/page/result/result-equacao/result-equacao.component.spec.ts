import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultEquacaoComponent } from './result-equacao.component';

describe('ResultEquacaoComponent', () => {
  let component: ResultEquacaoComponent;
  let fixture: ComponentFixture<ResultEquacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultEquacaoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResultEquacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
