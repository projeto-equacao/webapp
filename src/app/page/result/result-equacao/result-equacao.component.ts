import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { EquacaoService } from 'src/app/servico/equacao.service';
import { EquacaoResponse } from '../../../dto/response/equacao-response.dto';
import { EquacaoRequest } from '../../../dto/request/equacao-request.dto';

@Component({
  selector: 'result-equacao',
  templateUrl: './result-equacao.component.html',
  styleUrls: ['./result-equacao.component.scss']
})

export class ResultEquacaoComponent implements OnInit {

  public response: EquacaoResponse = {};
  public coeficientes: EquacaoRequest;
  
  constructor(private router: Router, private equacaoService: EquacaoService) {
    this.coeficientes=this.router.getCurrentNavigation()?.extras.state as {a: number, b: number, c: number}
  }
  
  ngOnInit(): void {
      this.equacaoService.calcular(this.coeficientes)
      .subscribe((response: EquacaoResponse ) => this.response = response )
  }

  titulo = 'Conjunto solução';
  
  logo = [
    'WHPHW - Encontrando os valores de X1 e X2'
  ];
  
  msg() {
    alert(this.response.message);
 }

}