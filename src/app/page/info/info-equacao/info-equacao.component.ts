import { Component } from '@angular/core';

@Component({
  selector: 'app-info-equacao',
  templateUrl: './info-equacao.component.html',
  styleUrls: ['./info-equacao.component.scss']
})
export class InfoEquacaoComponent {
  titulo = 'Agradecemos pela preferência';
  logo = [
    'WHPHW - Waldir Chagas Leite - A.D.S. - FullStak'
  ];
}
