import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoEquacaoComponent } from './info-equacao.component';

describe('InfoEquacaoComponent', () => {
  let component: InfoEquacaoComponent;
  let fixture: ComponentFixture<InfoEquacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoEquacaoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InfoEquacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
