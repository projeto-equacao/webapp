import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EquacaoRequest } from 'src/app/dto/request/equacao-request.dto';

@Component({
  selector: 'app-lancar-equacao',
  templateUrl: './lancar-equacao.component.html',
  styleUrls: ['./lancar-equacao.component.scss']
})

export class LancarEquacaoComponent {

  constructor(private router:Router) {}

  calcular() {
    this.router.navigate(['/result-equacao'], {state:this.coeficientes})
    }

  titulo = 'Nos campos abaixo informe os valores';
  logo = [
    'WHPHW - Encontar os valores do conjunto solução - { X1 e X2 }.'
  ];

  coeficientes: EquacaoRequest = {
    a: 0,
    b: 0,
    c: 0
  };

}
