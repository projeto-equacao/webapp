import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LancarEquacaoComponent } from './lancar-equacao.component';

describe('LancarEquacaoComponent', () => {
  let component: LancarEquacaoComponent;
  let fixture: ComponentFixture<LancarEquacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LancarEquacaoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LancarEquacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
